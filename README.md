# RadWende

## Verein zur Förderung von Lastenrädern im Straßenverkehr

RadWende ist ein Verein, der sich für die Förderung von Lastenrädern im Straßenverkehr um die Verbesserung der Fahrradinfrastruktur in Städten einzufordern. Unsere Hauptziele sind die Erhöhung der Sichtbarkeit von Radverkehr im Straßenbild, die Entschleunigung des Verkehrs und die Steigerung der Verkehrssicherheit in urbanen Gebieten.

Unsere Aktivitäten umfassen die Zusammenarbeit mit städtischen Entscheidungsträgern und Verkehrsplanern, die Organisation von Veranstaltungen und Workshops zum Thema Lastenräder und Fahrradinfrastruktur sowie die Umsetzung von Marketingmaßnahmen, wie zum Beispiel die Erstellung von ansprechenden Videos und die Nutzung von Social Media.

RadWende verfolgt das Ideal einer nachhaltigen, sicheren und effizienten Mobilität in Städten, bei der Lastenräder eine zentrale Rolle spielen. Unser Verein strebt danach, die Lebensqualität in urbanen Räumen zu erhöhen und zu einer umweltfreundlicheren Verkehrsgestaltung beizutragen.
  
Finanzierung: Jedes auf der Straße genutzte Lastenrad soll einen bestimmten Betrag generieren, der in einen Fond fließt. Die in einem Jahr gesammelten Geldbeträge werden der Stadt Wien zur Verfügung gestellt, um konkrete Infrastrukturprojekte für Fahrräder zu finanzieren. Zur Ausstattung des Fonds und zur Finanzierung der Vereinsprojekte sollen auch Spenden über Crowdfunding gesammelt werden.  
  
Zusammenarbeit: Der Verein soll eng mit der Stadt Wien und anderen Partnern zusammenarbeiten, um die Umsetzung von Fahrradinfrastrukturprojekten voranzutreiben. Hierbei sollen auch die Bedürfnisse von Lastenradfahrern berücksichtigt werden.  
  
Mitglieder: Der Verein soll aus einer breiten Mitgliederbasis bestehen, die das gemeinsame Ziel einer fahrradfreundlichen Stadt verfolgt. 
  
Öffentlichkeitsarbeit: Um das Thema Fahrradfahren und Lastenräder in der Öffentlichkeit stärker zu präsentieren, soll der Verein regelmäßig Veranstaltungen und Aktionen durchführen. Hierzu können beispielsweise Radtouren oder Lastenrad-Rallyes gehören.  

---


1.  Name des Vereins: "RadWende"
    
2.  Zielsetzung: 
 - a) Förderung der Nutzung von Lastenrädern, um den Radverkehr im Straßenbild sichtbarer zu machen 
 - b) Entschleunigung und Erhöhung der Verkehrssicherheit in Städten 
 - c) Sammlung von Geldern für den Ausbau von Fahrradinfrastruktur in Städten
    
3.  Struktur: 
 - a) Vorstand (Präsident, Vizepräsident, Schatzmeister, Schriftführer) 
 - b) Mitglieder (reguläre Mitglieder, Fördermitglieder) 
- c) Arbeitsgruppen (z.B. Infrastrukturprojekte, Crowdfunding, Marketing)

4. Finanzierung:
 - a) Jedes Lastenrad auf der Straße generiert Euro, die in einem Fonds gesammelt werden
 - b) Crowdfunding-Kampagnen zur Finanzierung von Infrastrukturprojekten und Vereinsaktivitäten 
 - c) Mitgliedsbeiträge und Spenden
    
5.  Zusammenarbeit mit der Stadt Wien: 
 - a) Die in einem Jahr gesammelten Geldbeträge werden der Stadt Wien zur Verfügung gestellt 
 -  b) Verein schlägt konkrete Infrastrukturprojekte für Fahrräder vor 
 - c) Regelmäßiger Austausch und Zusammenarbeit mit städtischen Entscheidungsträgern und Verkehrsplanern
    
6.  Marketing und Öffentlichkeitsarbeit: 
 - a) Erstellung von ansprechenden Videos, die Lastenräder im Straßenverkehr zeigen 
 - b) Nutzung von Social Media und anderen digitalen Plattformen zur Verbreitung der Inhalte und Steigerung der Bekanntheit des Vereins 
 - c) Veranstaltungen und Workshops zum Thema Lastenräder und Fahrradinfrastruktur 
 - d) Kooperationen mit lokalen Fahrradgeschäften, Unternehmen und Organisationen, um den Verein und seine Ziele zu unterstützen
    
7.  Monitoring und Evaluierung: 
 - a) Regelmäßige Überprüfung und Anpassung der Ziele und Strategien des Vereins 
 - b) Dokumentation und Analyse der erreichten Fortschritte und der Verwendung der gesammelten Gelder 
 - c) Erfolgsgeschichten und Best-Practice-Beispiele teilen, um andere Städte und Gemeinden zur Nachahmung zu motivieren
